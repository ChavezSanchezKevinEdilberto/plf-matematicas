# ACTIVIDAD: "Matemáticas"
# TEMA: "Conjuntos, Aplicaciones y funciones (2002)"
```plantuml
@startmindmap
*[#Orange] Matemáticas
	*[#Pink] Conjuntos
		*_ Definiciones
			*[#98FB98] Colección de elementos	
			*[#98FB98] Se da por entendido
				*[#lightblue] Relación de pertenencia
		*[#98FB98] Conceptos primitivos
			*[#lightblue] Inclusión
				*_ Esto ocurre cuando
					*[#3ad2ff] Un conjunto está contenido si todo elemento del primer conjunto pertenece al otro
					*[#3ad2ff] Propiedades abstractas 
		*[#98FB98] Operaciones 
			*[#lightblue] Conjuntos más complejos
				*_ Los que se encuentran son: 
					*[#3ad2ff] Intersección de conjuntos
						*[#FFFAFA] Elementos que pertenecen simultáneamente a ambos
					*[#3ad2ff] Unión de conjuntos
						*[#FFFAFA] Elementos que pertenecen al menos en uno de esos
					*[#3ad2ff] Complementación de conjuntos
						*[#FFFAFA] Elementos que no pertenecen a un conjunto dado
					*[#3ad2ff] Diferencia de conjuntos
						*_ Ocurre cuando el resultado es 
							*[#FFFAFA] Otro conjunto con los elementos del primer \nconjunto sin los elementos del segundo conjunto
		*[#98FB98] Conjunto universal
			*[#lightblue] Conjunto formado por todos los objetos de estudio en un contexto dado
			*[#lightblue] Conjunto de referencia donde ocurre toda la teoría
		*[#98FB98] Conjunto vacío
			*[#lightblue] Necesidad lógica para cerrar bien las cosas
			*[#lightblue] No contiene elementos
		*[#98FB98] Representaciones gráficas
			*[#lightblue] Diagramas de Venn
				*[#3ad2ff] Colección de elementos
				*_ Representados por 
					*[#3ad2ff] Circulos
					*[#3ad2ff] Óvalos
					*[#3ad2ff] Elementos encerrados
				*_ Implementado por 
					*[#3ad2ff] John Venn
						*_ En el siglo
							*[#FFFAFA] 1881
				*[#3ad2ff] No sirve para las demostraciones
		*[#98FB98] Cardinal de un conjunto
			*[#lightblue] Número de elementos que conforman el conjunto
				*[#3ad2ff] Números naturales
			*_ Características 
				*[#lightblue] Fórmula de la cardinal de la unión de conjuntos
					*[#3ad2ff] La cardinal de una unión es igual a la cardinal de conjunto A \nmás el cardinal del conjunto B menos el cardinal de la intersección
				*[#lightblue] Acotación de cardinales
					*[#3ad2ff] Mayor de cardinales de conjuntos
					*[#3ad2ff] Menor de cardinales de conjuntos
					*[#3ad2ff] Igual de cardinales de conjuntos
	*[#Pink] Transformación
		*[#98FB98] Historia del cambio
			*[#lightblue] Identificar cual elemento se convierte en el segundo
		*[#98FB98] Aplicación
			*[#lightblue] Convierte a cada uno de los elementos del conjunto \norigen en un único elemento de un conjunto final
			*[#lightblue] Todos los elementos del primer conjunto deben de \ntener un transformado único del segundo conjunto
			*_ Tipos
				*[#lightblue] Imagen inversa
					*[#3ad2ff] Es la aplicación que a cada subconjunto del conjunto final \nde la aplicación le hace corresponder el conjunto de elementos \ndel conjunto inicial cuya imagen se encuentra en este conjunto
				*[#lightblue] Imagen o imagen directa
					*[#3ad2ff] Hacer referencia al valor que le corresponde bajo la función
				*[#lightblue] Aplicación inyectiva
					*[#3ad2ff] Si dos elementos tienen la misma imagen, entonces necesariamente son el mismo elemento
				*[#lightblue] Aplicación sobreyectiva
					*[#3ad2ff] Si es equivalentemente
				*[#lightblue] Aplicación biyectiva
					*[#3ad2ff] Si es inyectiva y sobreyectiva
	*[#Pink] Función
		*[#98FB98] Aplicación de números
			*[#lightblue] Fáciles de visualizar
		*[#98FB98] Sistema de coordenadas
			*[#lightblue] Gráfica de la función
				*[#3ad2ff] Línea recta
				*[#3ad2ff] Serie de puntos
				*[#3ad2ff] Parábolas
@endmindmap
```
# TEMA: "Funciones (2010)"
```plantuml
@startmindmap
*[#lightblue] Funciones
	*_ Definiciones
		*[#98FB98] Es una regla de correspondencia entre dos conjuntos de tal manera \nque a cada elemento del primer conjunto le corresponde uno \ny sólo un elemento del segundo conjunto
			*_ Principalmente 
				*[#FFFAFA] Conjunto de los números reales
			*_ Ejemplo 
				*[#FFFAFA] Regla que cambia al conjunto de números reales a otro conjunto de números
				*[#FFFAFA] Cambio de divisas en la economía
				*[#FFFAFA] La forma en la que actúa el sol ante un muñeco de nieve
	*[#98FB98] Representaciones gráficas
		*[#FFFAFA] Representación cartesiana
			*_ Inventada por 
				*[#Pink] René Descartes
			*[#Pink] Conjunto de números reales
				*[#lightblue] Ejes de números reales
					*_ Eje de las 
						*[#3ad2ff] x
						*[#3ad2ff] y
					*_ Obtenemos 
						*[#3ad2ff] Imágenes
	*[#98FB98] Tipos de funciones
		*[#FFFAFA] Funciones crecientes
			*[#Pink] Aumenta la variable independiente
				*_ De igual forma sus
					*[#3ad2ff] Imágenes
		*[#FFFAFA] Funciones decrecientes
			*[#Pink] Variables que disminuyen
				*_ De igual forma sus
					*[#3ad2ff] Imágenes
	*[#98FB98] Comportamiento de las funciones
		*[#FFFAFA] Máximos
			*[#Pink] La función alcanza el mayor valor
		*[#FFFAFA] Mínimos
			*[#Pink] La función alcanza el mínimo valor
		*[#FFFAFA] Límite
			*_ Hace referencia a todas aquellas 
				*[#Pink] Divisiones existentes
		*[#FFFAFA] Continuidad
			*[#Pink] En la gráfica es una línea seguida, no interrumpida
		*[#FFFAFA] Discontinuidad
			*[#Pink] Puntos donde el denominador es 0, y es continua en cualquier otra parte
		*[#FFFAFA] Intervalo
			*[#Pink] Conjunto de los valores que toma una magnitud entre dos límites dados
		*[#FFFAFA] Dominio
			*[#Pink] Conjunto de todos los valores de entrada que al aplicar la función llevan a un valor de salida
		*[#FFFAFA] Derivada
			*[#Pink] Aproximaciones
				*[#FFFAFA] Resolver el problema de la aproximación de una función compleja por medio de una función línea
			*_ Apariciones en las ramas de 
				*[#Pink] Economía
				*[#Pink] Ciencias sociales
				*[#Pink] Ingeniería
			*_ Reglas
				*[#Pink] La derivada de una suma
				*[#Pink] La derivada de una constante por una función
				*[#Pink] La derivada de una potencia entera positiva
				*[#Pink] La derivada de una constante
				*[#Pink] La derivada de un producto
				*[#Pink] La derivada de un cociente
				*[#Pink] Las derivadas de las funciones trigonométricas
				*[#Pink] La regla de la cadena
@endmindmap
```
# TEMA: "La matemática del computador (2002)"
```plantuml
@startmindmap
*[#Orange] Aritmética del computador
	*[#FFFAFA] Cálculos
		*[#lightblue] Deducción lógica
			*_ Contiene
				*[#98FB98] Valores abstractos
			*[#98FB98] Resolución de problemas cotidianos
		*[#lightblue] Desarrollo del computador

		*[#lightblue] Arítmetica de presición finita
			*[#lightblue] Aproximación de un número
			*[#lightblue] Infinitas cifras
				*[#98FB98] Números reales
					*_ Ejemplo 
						*[#Pink] sqrt(2)
				*[#98FB98] Números fraccionales
					*_ Ejemplo 
						*[#Pink] 1/3
				*[#98FB98] Números decimales
					*_ Ejemplo 
						*[#Pink] 0.333...
			*_ Técnicas
				*[#98FB98] Truncamiento
					*[#Pink] Cortar las decimales para una aproximación
					*[#Pink] Despreciar una cierta cantidad de cifras
				*[#98FB98] Redondeo
					*[#Pink] Despreciarlas y retocar la última cifra
					*[#Pink] Evitar errores con los números
				*[#98FB98] Mantisa
					*_ Ecnontrado en los 
						*[#Pink] Logaritmos
					*[#Pink] Diferencia entre un número y su parte entera
						*[#DDA0DD] Fraccionaria
			*[#lightblue] Cifras significativas
				*[#98FB98] Representan el uso de una o más escalas de \nincertidumbre en determinadas aproximaciones
				*_ Ejemplo
					*[#98FB98] Se dice que 4,7 tiene dos cifras significativas, \nmientras que 4,07 tiene tres
	*[#FFFAFA] Sistema de númeración
		*[#lightblue] Sistema decimal
			*[#98FB98] Base aritmética el número diez
		*[#lightblue] Sistema binario
			*[#98FB98] Dígitos binarios
				*[#Pink] Base 2
					*[#DDA0DD] 0 y 1
			*[#98FB98] Símbolos
			*[#98FB98] Realizar operaciones
				*[#Pink] Adición binaria
					*[#DDA0DD] Sumar números
			*_ Usos 
				*[#98FB98] Imágenes
				*[#98FB98] Videos
				*[#98FB98] Aspectos digitales
			*_ Aplicaciones 
				*[#98FB98] Interruptores
					*[#DDA0DD] 0 No pasar la corriente
					*[#DDA0DD] 1 Paso de corriente
				*[#98FB98] Circuitos
				*[#98FB98] Corrientes
		*[#lightblue] Sistema octal
			*[#98FB98] Base 8
				*[#Pink] 0,1,2,3,4,5,6 y 7
			*[#98FB98] Usado en la informática
			*[#98FB98] No requiere utilizar otros símbolos diferentes de los dígitos
		*[#lightblue] Sistema hexadecimal
			*[#98FB98] Posicional
			*[#98FB98] Base 16
		*[#lightblue] Aritmética en punto flotante
			*[#98FB98] Todos los cálculos se realizan con una precisión limitada
		*[#lightblue] Desbordamiento
			*[#98FB98] Fallo informático que se da cuando 
				*_ Se da cuando el 
					*[#Pink] Código almacenado en un registro supera su valor máximo
@endmindmap
```
## Autor: Chávez Sánchez Kevin Edilberto

[ChavezSanchezKevinEdilberto @ GitLab](https://gitlab.com/ChavezSanchezKevinEdilberto)

### Para la creación de estos Para la creación de tus mapas mentales harás uso de los formatos Markdown y PlantUML.
#### Herramientas
#### Markdown
[markdown ](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts)
#### PlantUML
[plantuml ](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
#### MindMap
[mindmap ](https://plantuml.com/es/mindmap-diagram)